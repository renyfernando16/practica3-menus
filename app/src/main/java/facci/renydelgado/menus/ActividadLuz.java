package facci.renydelgado.menus;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Camera;
import android.os.Bundle;
import android.widget.Button;

public class ActividadLuz extends AppCompatActivity {

    Button btnEncenderLinterna;
    Camera camara;
    private boolean estaEncendido = false;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_luz);

        btnEncenderLinterna = (Button) findViewById(R.id.btnEncenderLuz);
        
        btnEncenderLinterna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                    String cameraId = camManager.getCameraIdList()[0]; // usualmente la camara delantera esta en la posicion 0
                    camManager.setTorchMode(cameraId, true);
                    
                }

                catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }
        });





    }
}

/*

try {
                    CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                    String cameraId = camManager.getCameraIdList()[0]; // usualmente la camara delantera esta en la posicion 0
                    camManager.setTorchMode(cameraId, true);
                    camManager
                }

                catch (CameraAccessException e) {
                    e.printStackTrace();
                }
 */