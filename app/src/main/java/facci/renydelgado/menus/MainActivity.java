package facci.renydelgado.menus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import facci.renydelgado.menus.ui.login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflacter = getMenuInflater();
        inflacter.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Intent intent;
        switch (item.getItemId()){

            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcionAcelerometro:
                intent = new Intent(MainActivity.this, ActividadAcelerometro.class);
                startActivity(intent);
                break;
            case R.id.opcionProximidad:
                intent = new Intent(MainActivity.this, ActividadProximidad.class);
                startActivity(intent);
                break;
            case R.id.opcionLuz:
                intent = new Intent(MainActivity.this, ActividadLuz.class);
                startActivity(intent);
                break;
            case R.id.opcionVibracion:
                intent = new Intent(MainActivity.this, ActividadVibracion.class);
                startActivity(intent);
                break;
            case R.id.opcionLinterna:
                intent = new Intent(MainActivity.this, ActividadLinterna.class);
                startActivity(intent);
                break;
        }
        return true;

    }
}
